clc
echo on
%
%Esercizio 1
%

%Definizione del sistema dinamico
A=[0.5,0.5;-0.5,0.5];
B=[0.5;0.5];
C=[1,1];
D=1;

s1=ss(A,B,C,D,-1)
pause

%Visualizzazione poli e zeri
pzmap(s1),zgrid
pause

%Guadagno statico
dcgain(s1)
C*inv(eye(2)-A)*B+D
pause

%Risposta allo scalino
step(s1)
pause

%
%Esercizio 2
%

%Definizione delle f.d.t.
p=0.3;G21=tf(1-p,[1 -p],-1)
p=0.6;G22=tf(1-p,[1 -p],-1)
p=0.9;G23=tf(1-p,[1 -p],-1)
p=-0.3;G24=tf(1-p,[1 -p],-1)
pause

%Risposte allo scalino
step(G21,G22,G23,G24)
pause

%Calcolo risposta in frequenza
p=0.3;
G21_1=freqresp(G21,1)
pause

%Risposta alla sinusoide
k=(0:20)';
u=sin(k);
y=lsim(G21,u,k);
pause

%Risposta asintotica
yappr=abs(G21_1)*sin(k+angle(G21_1));
pause

%Confronto
stairs(k,[u y yappr])
pause

%
%Esercizio 3
%

%Definizione campioni
k=-50:50; 
vstar=sin(k)+sin(2*k);
pause

%Calcolo con la formula di Shannon
addendi=vstar.*sin(pi*0.3-k*pi)./(pi*0.3-k*pi);
sum(addendi)
pause

%Calcolo esatto
sin(0.3)+sin(2*0.3)
pause

%Visualizzazione addendi
plot(k,addendi)




