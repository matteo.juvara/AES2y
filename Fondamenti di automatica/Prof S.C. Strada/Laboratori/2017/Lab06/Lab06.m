close all

%% 1
A = [0.5 0.5; -0.5 0.5];
B = [0.5 ; 0.5];
C = [1 1];
D = 1;

% 1.1
sys = ss(A,B,C,D,-1)
[z,p,k] = zpkdata(sys,'v')

if (max(abs(p))) < 1
   disp('Sist. Asint. Stab.') 
end


% Analiticamente
mu = C * inv( (eye(length(diag(A))) - A) ) * B + D

% Comando specifico
mu = dcgain(sys)

step(sys)
grid on


%% 2

% 2.1
z = tf('z');
p_vect = [-0.3 0.3 0.6 0.9];

figure
for k = 1:length(p_vect)
    p = p_vect(k);
    G = (1-p)/(z-p);
    
    subplot(2,2,k)
    step(G)
    title(sprintf('Step (p=%.1f)', p))
    grid on
end


%2.2

figure

k = [0:100];
theta = 1;
y = lsim(G,sin(theta*k),k)

G1 = freqresp(G,theta)
y_as = abs(G1)*sin(theta*k + angle(G1));
plot(k,y);
hold on
plot(k, y_as)
grid on

legend('y(k)','y_{as}(k)')
title('Verifica th r.i.f')



%% 3
k = [-500:500];
omega_N = 0.3;
v_star = sin(k)+sin(2*k);
shannonTerms = v_star.*sin(pi*omega_N - k*pi)./(pi.*omega_N - k*pi);
plot(k, shannonTerms)
grid on
hold on

v_omegaN_approx = sum(shannonTerms)
v_omegaN = sin(omega_N)+sin(2*omega_N)



