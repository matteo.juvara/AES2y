
% x = [u w q theta h]'; y=h;

close all

A = [-0.00643 0.0263 0 -32.2 0;
     -0.0941 -0.624  820 0   0;
     -0.000222  -0.00153 -0.668 0 0;
     0  0   1   0   0 ;
     0  -1  0   830 0]
B = [0 -32.7 -2.08 0 0]'
C = [0 0 0 0 1]
D = 0



%% a
sys = ss(A,B,C,D);
b747 = tf(sys)

%% b
damp(b747)
[z,p,k] = zpkdata(b747,'v') %zeros, poles, transfer constant

%% c
figure
poles_Re = real(p);
poles_Im = imag(p);
plot(poles_Re, poles_Im, 'x');
hold on
grid on
zeros_Re = real(z);
zeros_Im = imag(z);
plot(zeros_Re, zeros_Im, 'o');
title('Poles and Zeros')

% Alternatively, just: pzmap(b747)

figure
step(b747)
grid on
figure
impulse(b747)
grid on

figure
bode(b747)
grid on
figure
nyquist(b747)
grid on

%% d
[k_m_b747, phi_m_b747,~,~] = margin(-b747) % Gain and Phase Margins of the system under control

%% e
K_p = 5*1e-5;
K_d = 10*K_p;

[k_m_CL, phi_m_CL,~,~] = margin(series( b747, tf(-[K_d K_p],1))) % Gain and Phase Margins of the Closed Loop system

margin(series( b747, tf(-[K_d K_p],1)))
%% f
step(feedback( series( b747, tf(-[K_d K_p],1)), 1 ))
grid on



%% Project Specifications

L = series( b747, tf(-[K_d K_p],1))
F = 1/(1+L)
S = L/(1+L)


e_inf = dcgain(S)

xi = phi_m_CL/100


[~,phi_m, ~, w_c] = margin(L);
w_n = w_c; % approx.
t_a = 5/(0.5*w_n)





