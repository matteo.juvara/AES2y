close all
%% 2.1
s = tf('s');
G = 1/(s^2+1.4*s+1);

figure
subplot(1,3,1)
bode(G)
omega_b = bandwidth(G)
grid on

%% 2.2
subplot(1,3,2)
u = @(t) sin(0.4*t)+0.2*sin(10*t);
t = [0:0.01:100];
plot(t,u(t))
grid on
hold on

y = lsim(G,u(t),t);
plot(t,y)

legend('u(t)', 'y(t)')

%% 2.3
[u,t] =  gensig('square', 15); % Onda quadra con periodo T=15s

subplot(1,3,3)
plot(t,u)
grid on
hold on

y = lsim(G,u,t);
plot(t,y)
grid on

legend('u(t)', 'y(t)')


%% 2.4 (IL CODICE E' IDENTICO A SOPRA, CAMBIA SOLO LO SMORZAMENTO DI G)
figure

s = tf('s');
G = 1/(s^2+0.1*s+1);

subplot(1,3,1)
bode(G)
omega_b = bandwidth(G)
grid on

%
subplot(1,3,2)
u = @(t) sin(0.4*t)+0.2*sin(10*t);
t = [0:0.01:100];
plot(t,u(t))
grid on
hold on

y = lsim(G,u(t),t);
plot(t,y)

legend('u(t)', 'y(t)')

%
[u,t] =  gensig('square', 15); % Onda quadra con periodo T=15s

subplot(1,3,3)
plot(t,u)
grid on
hold on

y = lsim(G,u,t);
plot(t,y)
grid on

legend('u(t)', 'y(t)')



%% OSS. Proviamo ad usare un ingresso sinusoidale con pulsazione molto vicina a quella di risonanaza
% Prendo omega=1
figure

u = @(t) sin(t);
t = [0:0.01:100];
plot(t,u(t))
grid on
hold on

y = lsim(G,u(t),t);
plot(t,y)

legend('u(t)', 'y(t)')

