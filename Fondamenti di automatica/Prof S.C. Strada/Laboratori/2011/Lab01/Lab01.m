clc
echo on

%%%
%%% ESERCIZIO 1
%%%

%Definizione del sistema dinamico
A=[-4,-3,0,3;0,-3,-1,1;0,-2,-3,2;0,-2,-1,0];
B=[0;0;0;1];
C=[1,1,0,0];
s1=ss(A,B,C,0);
pause
		
%Verifica della stabilit�
eig(s1)

%Il sistema � asintoticamente stabile
pause

%Calcolo del moto libero
x0=[1;0;1;2];
initial(s1,x0)

pause

%Calcolo del guadagno statico
dcgain(s1)
pause

-C*inv(A)*B;
pause

%Calcolo della risposta allo scalino
step(s1)
pause


%%%
%%% ESERCIZIO 2
%%%


%Definizione del sistema dinamico
A=[0,0,1;0,0,1;-1,-1,-1];
B=[0;0;1];
C=[0,0,1];
s2=ss(A,B,C,0);
pause

%Matrice di raggiungibilit�
Kr=ctrb(s2);
det(Kr)
%Il sistema non � completamente raggiungibile
pause



%Matrice di osservabilit�
Ko=obsv(s2);
det(Ko)
%Il sistema non � completamente osservabile
pause

%Funzione di trasferimento
tf(s2)

%La f.d.t. � quella di un sistema del secondo ordine (a parte l'errore numerico)
pause

%Prendiamo come nuove variabili somma e differenza delle tensioni sui condensatori e la corrente sull'induttore

T=[1,-1,0;1,1,0;0,0,1];
s2new=ss2ss(s2,T);
[A2,B2,C2,D2]=ssdata(s2new);

%La prima variabile di stato non contribuisce al legame ingresso-uscita
pause


tf(s2new)
%La funzione di trasferimento � la stessa
pause

%%%%
%%% ESERCIZIO 3
%%%

G1=tf(3,[2 1]);
G2=tf(3*[1 1],[2 1]);
G3=tf(3*[-1 1],[2 1]);
pause

step(G1,G2,G3)
pause

G4=tf(3,[10 11 1]);
G5=tf(3,[10 1]);
pause

step(G4,G5)
pause

G6=tf(3,[100 20 1]);
G7=tf(3*[30 1],[100 20 1]);
G8=tf(3*[-30 1],[100 20 1]);
pause

step(G6,G7,G8)
pause

%%%
%%% ESERCIZIO 4
%%%

%Calcolo della pulsazione naturale
wn=log(100)/(10*0.2);

%Definizione della funzione di trasferimento
G=tf(3*wn^2,[1 0.4*wn wn^2]);

%Risposta allo scalino
step(G)
pause

%Calcolo della sovraelongazione percentuale massima
S=exp(-0.2*pi/sqrt(1-0.2^2));