M = 63000;
v = 60;
R = 0.55;
g = 9.807;
mu = 0.9;
r_p = 0.9;

c_v = 500;
T_max = 500;

% Steel
T_a = 35;
etha = 0.75;
rho = 7800;
s_d = 0.01;
%

N = 4;

E_k = 0.5*M*v^2
m = (etha*E_k)/(c_v*(T_max-T_a))

V_min = (m/rho)/N

D_1 = 0.25;
D_2 = 0.65;
A_d = (pi/4)*(D_2^2 - D_1^2)

n = ceil( V_min/(A_d*s_d) ) %numero dischi

V_tot = A_d*s_d*n*N;
m_tot = V_tot*rho
T_max_d = etha*E_k/(m_tot*c_v) + T_a
