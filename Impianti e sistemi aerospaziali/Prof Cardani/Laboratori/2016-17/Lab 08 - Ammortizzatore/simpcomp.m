function [ I ] = simpcomp( a, b, M, f )

H = (b-a)/M;

x_vect = linspace(a,b,M+1);

x_med_vect = x_vect(1:length(x_vect)-1) + (H/2)*ones(1,length(x_vect)-1);

sumSimp = [];

for k = 1:M
    sumSimp = [sumSimp (f(x_vect(k)) + 4*f(x_med_vect(k)) + f(x_vect(k+1)))];
end

I = (H/6)*sum(sumSimp);



return

