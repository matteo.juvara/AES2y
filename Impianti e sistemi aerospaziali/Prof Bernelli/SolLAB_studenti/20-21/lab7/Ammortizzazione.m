clc, clear, close all
%% DATI
m = 67000;      %[kg] massa velivolo
vz = 3.2;       %[m/s] Componente vertical della velocit all'atterraggio
n = 2.5;        %fattore di carico
eta = 0.88;     %rendimento ammortizzatore
rap_d = 0.72;	%rapporto dischi
p0 = 4*10^6;	%[Pa] pressione di precarica
pm = 12*10^6;	%[Pa] pressione a schiacciamento massimo
ksi = 2;        %coeff perdite carico nell luci di passaggio
ro = 850;       %[kg/m^3] densità olio
g = 9.81;       %[m/s^2] acc. gravitazionale
gamma = (7/2)/(5/2);   % indice politropica(Cp/Cv), Azoto è biatomico
% 0.90 è la parte di peso sul carrello principale sul totale staticamente
%% A:  Dc, Ds, V0
% A.1
Rmax = (n-1)*m*g;   %[N] totale peso che si scarica sul carrello principale 
                    % all'atterraggio
Rm = Rmax/2;        %[N] Peso scaricato su una gamba (due gambe)
As = Rm/pm;         %[m^2] area stelo
Ds = sqrt(4*As/pi); %[m] diametro stelo
Dc = Ds/rap_d;      %[m] diametro cilindro
Ac = Dc^2*(pi/4);   %[m^2] area cilindro
delta = vz^2/(2*g*(eta*(n-1)-(1/3))); %[m] corsa massima in metri
deltaV = As*delta;                    %[m^3] volume di gas schiacciato
V0 = deltaV/(1-(p0/pm)^(1/gamma));    %[m^3] Volume di gas alla precarica 
h = V0/Ac;                            %[m] altezza in metri

fprintf('\n\tA. Diametri Stelo-Cilindro e Volume precarica\n Dc = %f m\n Ds = %f m\n Vo = %f m^3\n',Dc,Ds,V0);

%% B: Xs (condizioni statiche)

Rs = 0.90*m*g/2;	%[N] reazione in condizioni statiche
ps = Rs/As;         %[Pa] pressione in condizioni statiche all'interno 
                    % del martinetto
Xs = (V0/As)*(1-(p0/ps)^(1/gamma)); %[m] corsa del martinetto in 
                                    % condizioni statiche
fprintf('\n\tB. Corsa in condizioni statiche\n Xs = %f m\n',Xs);

%% C: Area Fori
% al 30% della corsa voglio massimizzare l'azione di ammortizzazione
% grazie ai trafilamenti (aggiungendosi al contributo di gas) 
Xd = 0.3 * delta;   %[m] corsa al 30%
vd = 0.75*vz;       %[m/s] velocità relativa alla corsa al 30%
p1 = p0*(V0/(V0-As*Xd))^gamma; %[Pa] pressione del gas alla corsa Xd

% Caso Teorico ipotozzato (nella realtà posso avere cavitazione)
dp = (Rm-p1*As)/(Ac-As);       %[Pa] pressione in aggiunta da fornire per 
                               % poter eguagliare reazione massima Rm
v = sqrt(dp/(ksi*0.5*ro));     %[m/s] velocità dell'olio di trafilamento
Q = (Ac-As)*vd;                %[m^3/s] portata di trafilamento
Af = Q/v * 10^4;      %[cm^2] Area totale

% 2 fori: 
Af_singolo2 = Af / 2;
% 4 fori: 
Af_singolo4 = Af / 4;
fprintf('\n\tC. Area delle luci di passaggio dell''olio teorica\n Af = %f cm^2\n',Af);

% caso reale

dp_r = p1;                     %[Pa] pressione in aggiunta da fornire per 
                               % poter eguagliare reazione massima Rm
v_r = sqrt(dp_r/(ksi*0.5*ro)); %[m/s] velocità dell'olio di trafilamento

Ac_r = Rm/p1;                  %[m] Area Cilindro nuova da Rm = p1*Ac
Q_r = (Ac_r-As)*vd;            %[m^3/s] portata di trafilamento
Af_r = Q_r/v_r * 10^4;         %[cm^2] Area totale
 
fprintf('\n\tC. Area delle luci di passaggio dell''olio reale\n Af_r = %f cm^2\n',Af_r);



 



