function [Kij] = K(kij,ro,D)
% K converte k relativo alle velocità in k relativo alle portate
% [Kij] = K(kij,ro,D)
% k -> relativo alle concentrate/velocita
%   * k = lambda * L /D;
%   * k concentrata nota;
% ro -> densità in kg/m^3
% D -> diametro in metri

Kij = (8*kij*ro)/(pi^2 * D^4);

end

