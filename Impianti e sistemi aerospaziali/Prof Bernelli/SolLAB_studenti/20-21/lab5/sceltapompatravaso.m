clc
clear 
close all

%% DATI 

%COMBUSTIBILE
ro = 780; %[kg/m^3]

%MOTORE
Qmax = 5000/(ro * 3600); %[m^3/s]
Qmin = 1500/(ro * 3600); %[m^3/s]
pmin = 0.05*10^6; %[Pa]
pmax = 0.45*10^6; %[Pa]

Q = linspace(0,7000,1000)';%[m^3/s]
Q_disp = (linspace(Qmin,Qmax,100)')*(3600*ro);
%TUBAZIONI
vmax = 4.5; %[m/s]
l_AC = 0.5; %[m]
l_CB = 4; %[m]
l_CM = 14; %[m]
l_BM = 12; %[m]
lambda = 0.03;
k_AB = 5;
k_AM = 12;
k_BM = 8;

%% 1) Dimensioni del Tubo + Calcolo diagramma funzionamento
%sovradimensiono leggermente Qmax se no potrebbe succede che serbatoio B si svuoti
%al massimo poi calcolo diametro per diverse Q rendendolo un vettore


D = 2*sqrt((Qmax + (500/(ro * 3600))) / (pi * vmax)); %[m]
fprintf('Con sovradimensionamento di 500 kg/h, D = 2.35 cm\n')
%sovradimensiono di 500 perchè se no ci sarebbe il rischio che a 5000 il
%travaso non riesca a riempire il serbatoio B che finisce per svuotarsi.


%caso emergenza è più restrittivo, verificare funzionamento per caso
%emergenza poi verifico se funziona per il caso nominale.

k_am_dist = lambda * (l_AC + l_CM) / D;
K_emergenza = K(k_AM + k_am_dist,ro,D); 

dp_emergenza = K_emergenza .*linspace(Qmin,Qmax,100)'.^2; %[Pa]

pmin_emerg = pmin + dp_emergenza;
pmax_emerg = pmax + dp_emergenza;



Qm = (6000:500:8000); %[kg/h] 

% p_pompa = @(Q) pmax*(-1.7*(Q./Qm).^2 + 0.8*(Q./Qm) + 0.9);

P = [];
for l = Qm
   p_pompa = @(Q) (pmax) *(-1.7*(Q./l).^2 + 0.8*(Q./l) + 0.9); %[Pa]
   press = p_pompa(Q);
   P = [P press];
end

figure(1), plot(Q_disp,pmin_emerg,'r',Q_disp,pmax_emerg,'r',...
               [5000 5000], [248926 648926],'r', [1500 1500], [467903 67903.4],'r');
title('EMERGENZA')
xlabel('PORTATA [kg/h]')
ylabel('PRESSIONE [Pa]')
hold on
plot(Q,P(:,1),Q,P(:,2),Q,P(:,3),Q,P(:,4))
fprintf('La pompa con Pmax = %d Pa e Qm = 7000 kg/h è nell''area di funzionamento\n',pmax);

Pm_ = pmax;
Qm_ = 7500 / (ro * 3600); %[m^3/s]

%% CONDIZIONI NOMINALI
k_bm_dist = lambda * (l_BM) / D;
K_nominale = K(k_BM + k_bm_dist,ro,D); 
dp_nominale = K_nominale .*linspace(Qmin,Qmax,100)'.^2; %[Pa]

pmin_nom = pmin + dp_nominale;
pmax_nom = pmax + dp_nominale;

figure(2), plot(Q_disp,pmin_nom,'r',Q_disp,pmax_nom,'r',...
               [5000 5000], [202025 602025],'r', [1500 1500], [463682 63682.3],'r');
title('NOMINALE')
xlabel('PORTATA [kg/h]')
ylabel('PRESSIONE [Pa]')
hold on
plot(Q,P(:,3)) %%POMPA 3
fprintf('Come si può notare graficamente la pompa scelta va bene anche per il caso nominale\n');

%% TRAVASO
k_ab_dist = lambda * (l_AC + l_CB) / D;
K_travaso = K(k_AB + k_ab_dist,ro,D); 
dp_travaso = K_travaso .*linspace(0,7000/(ro * 3600),1000)'.^2; %[Pa]

figure(3), plot(Q,dp_travaso,Q,P(:,3))
title('TRAVASO')
xlabel('PORTATA [kg/h]')
ylabel('PRESSIONE [Pa]')

fprintf('\nDall''analisi grafica si evince che il punto di funzionamento\nsi trova alla p = 110864 Pa e alla Q = 6290 kg/h\n');

%se voglio calcolarlo, devo fare : sistema = @(Q) Q^2*((k_AB/pmax)+(1.7/Qm)) - Q*(0.8/Qm) -0.9
%Q_travaso = fzero(sistema) 


%% TEMPO MARCIA POMPA A
Autonomia = 12; %[h]
Consumo_med = 3000; %[kg/h]
Massa_carburante = Consumo_med*Autonomia; %[kg]
Volume_serbatoio = Massa_carburante / ro; %[m^3] considero i due serbatoi di stesso volume
fprintf('\nIl volume di un serbatoio vale: %f [m^3]\n',Volume_serbatoio);

% erogazione da b a Q = 3500
Q_in_b = (6290 - 3500); %[kg/h] Q_travaso = 6290
Mass = ((90 - 70)/100) * (Massa_carburante / 2); %massa da fornire.
tempo_m = Mass / Q_in_b; %[h]
tempo_arr = Mass / 3500;
fprintf('Il tempo di arresto della Pompa A vale: %f [h]\n',tempo_arr);
fprintf('Il tempo di marcia della Pompa A vale: %f [h]\n',tempo_m);

%% COMMENTO
%Il prof decide di maggiorare il diametro con 2.5cm e quindi anche la pompa
%con Qm = 6500 entra nell'area richiesta.