close all, clear all
%% 1.1
f = @ (x) sqrt((x-2).^7);
g = @ (x) sqrt(x-2);

a = 2;
b = 3;
x = linspace(a,b,10000);

plot(x,f(x))
hold on
grid on
plot(x,g(x))
legend('f','g')

%% 1.3, 1.4

M_vect = 5*2.^[0:4];

I_vect_f = [];
I_vect_g = [];

I_ex_f = 2/9;
I_ex_g = 2/3;

for k = 1:length(M_vect)
    M = M_vect(k);
    
    I_f = pmedcomp( a,b,f, M );
    I_vect_f = [I_vect_f; I_f];
    
    I_g = pmedcomp( a,b,g, M );
    I_vect_g = [I_vect_g; I_g];
    
end


I_vect_f
I_vect_g


E_f_vect = abs(I_ex_f - I_vect_f)
E_g_vect = abs(I_ex_g - I_vect_g)


figure
loglog(M_vect, E_f_vect)
grid on
hold on
loglog(M_vect, E_g_vect)
loglog(M_vect, M_vect.^(-2), '--k')
legend('E_{f}', 'E_{g}', 'M^{-2}')