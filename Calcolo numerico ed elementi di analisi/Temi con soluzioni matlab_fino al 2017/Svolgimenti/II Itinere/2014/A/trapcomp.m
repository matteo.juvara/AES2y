function [ I ] = trapcomp(a,b,f,M)

H = (b-a)/M;

x = linspace(a,b,M+1);

I = 0;

for k = 1:M
    
    I = I + (H/2)*( f(x(k)) + f(x(k+1)) );

end

