close all, clear all
%% 1.1
f = @ (x) sqrt((1-x).^5);
g = @ (x) sqrt(1-x);

a = -1;
b = 1;
x = linspace(a,b,10000);

plot(x,f(x))
hold on
grid on
plot(x,g(x))
legend('f','g')


d2f = @(x) - (10*(x - 1).^3)./(-(x - 1).^5).^(1/2) - (25*(x - 1).^8)./(4*(-(x - 1).^5).^(3/2));
d2g = @(x) -1./(4*(1 - x).^(3/2));

figure
plot(x,d2f(x))
grid on
legend('d^{2}f')

figure
plot(x,d2g(x))
grid on
legend('d^{2}g')




%% 1.3, 1.4

M_vect = 5*2.^[0:4];

I_vect_f = [];
I_vect_g = [];

I_ex_f =  sqrt(512)/7;
I_ex_g = sqrt(32)/3;

for k = 1:length(M_vect)
    M = M_vect(k);
    
    I_f = trapcomp( a,b,f, M );
    I_vect_f = [I_vect_f; I_f];
    
    I_g = trapcomp( a,b,g, M );
    I_vect_g = [I_vect_g; I_g];
    
end


I_vect_f
I_vect_g


E_f_vect = abs(I_ex_f - I_vect_f)
E_g_vect = abs(I_ex_g - I_vect_g)


figure
loglog(M_vect, E_f_vect)
grid on
hold on
loglog(M_vect, E_g_vect)
loglog(M_vect, M_vect.^(-2), '--k')
legend('E_{f}', 'E_{g}', 'M^{-2}')



p_appr = log(E_g_vect(end-1)/E_g_vect(end)) / log(M_vect(end-1)/M_vect(end))