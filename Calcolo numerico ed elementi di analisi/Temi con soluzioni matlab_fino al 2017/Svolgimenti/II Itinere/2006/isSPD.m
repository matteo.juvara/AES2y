function [ ] = isSPD( A )

n = length(diag(A));
lambdas = eig(A);

if (min(lambdas) > 0) & (A==A')
   disp('TRUE')
else
    disp('FALSE')
end

end

