%% 2.1
n = 20;
A = 4*eye(n) - diag(ones(n-1,1),-1) - diag(ones(n-1,1),1);
v = [1:n]';
C = v*v' + 0.1*A;

x_exact = ones(n,1);
%Ax=b -> b=A*x;
b=A*x_exact

z_exact = ones(n,1);
%Cz=d -> d=C*z;
d=C*z_exact


%% 2.2
% Ly=Pb
% Ux=y;
[L1,U1,P1] = lu(A);
y1 = fwsub(L1,P1*b);
x = bksub(U1,y1)

r1 = b-A*x;
r1_rel = norm(r1)/norm(b)
e1_rel = norm(x-x_exact)/norm(x_exact)


[L2,U2,P2] = lu(C);
y2 = fwsub(L2,P2*d);
z = bksub(U2,y2);

r2 = d-C*z;
r2_rel = norm(r2)/norm(d)
e2_rel = norm(z-z_exact)/norm(z_exact)


%% 2.3
condA = cond(A)
condC = cond(C)

% A ha un numero di condizionamento piccolo -> il residuo è uno stimatore
% affidabile dell'errore relativo (hanno infatti lo stesso ordine di grandezza)

% C ha un numero di condizionamento grande -> il residuo NOn è uno stimatore
% affidabile dell'errore relativo (e_rel è infatti superiore di quasi 4 ordini di grandezza)


%% 2.5
isSPD(A)
isSPD(C)
% Vediamo che entrambe le matrici sono SDP -> GS converge per ogni initial
% guess (TH)


%% 2.6
x0 = zeros(n,1);
z0 = zeros(n,1);
tol = 1e-5;
itermax = 1e+5;

t0 = tic;
[x,k_x] = gs(A,b,x0,tol,itermax)
t1 = toc(t0);

t2 = tic;
[z,k_z] = gs(C,d,z0,tol,itermax)
t3 = toc(t2);

time_x = t1
time_z = t3

% P=T=tril(A)
B1 = (eye(n) - inv(tril(A))*A);
rho_B1 = max(abs(eig(B1)))

% P=T=tril(C)
B2 = (eye(n) - inv(tril(C))*C);
rho_B2 = max(abs(eig(B2)))

% Vediamo subito che l'enorme differenza tra le velocità di convergenza di GS
% per i due sistemi è dovuta al fatto che per Ax=b il raggio spettrale della matrice di
% iterazione è piccolo (rho_B1 = 0.244446600723268), mentre per Cx=D il raggio spettrale
% è quasi unitario (rho_B2 = 0.999873542689395)




