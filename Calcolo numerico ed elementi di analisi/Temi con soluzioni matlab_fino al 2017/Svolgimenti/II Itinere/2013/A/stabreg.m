function stabreg(a,b)
%
%   Disegna la regione di assoluta stabilita' del metodo multistep
%   i cui coefficienti a(j) j=0,...p sono dati nel vettore colonna 
%   'a', mentre i b(j) j=-1,....,p sono dati nel vettore colonna 'b'.

r = exp(i*pi*(0:2000)/1000);
na = length(a);
nb = length(b);
if (nb ~= na+1)
    error('Dimensioni dei vettori non compatibili')
end
p = na-1;

rho = r.^(p+1);
sigma = b(1)*r.^(p+1);

for j = 0:p
    rho = rho - a(j+1)*r.^(p-j);
    sigma = sigma + b(j+2)*r.^(p-j);
end
plot(rho./sigma)


 

 




