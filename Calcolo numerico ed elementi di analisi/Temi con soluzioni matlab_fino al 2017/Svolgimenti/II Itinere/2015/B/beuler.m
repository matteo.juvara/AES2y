function [t_h, u_h] = beuler(f,T,y_0,h)

u_h = [];
t_h = [];
u = y_0;

n= T/h;


for k = 1:n-1
    t = k*h;
    u = (u + h*(-t*cosh(2*t) + 2*sinh(2*t)*(1-t))) / (1+h);
    
    u_h = [u_h; u];
    t_h = [t_h; t];
end



return

