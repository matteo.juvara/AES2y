close all
clear all

%% 4-5

f = @(t,y) t*y/(t-1).^2;
t0 = 2;
tf = 10;
y0 = 1;

h_vect = 2.^-[1:5];

y_ex = @(t) (t-1).*exp((t-2)./(t-1));

t_axis = [t0:0.001:tf];

u_f_vect = [];
e_h_vect = [];

figure
hold on
grid on
plot(t_axis, y_ex(t_axis));

for k = 1:length(h_vect)
    h = h_vect(k)
    
    
    [t_h, u_h] = feuler(f,t0,tf,y0,h);
    plot(t_h, u_h)
    
    u_f = u_h(end);
    u_f_vect = [u_f_vect; u_f];
    
    
    e_h = max(abs(y_ex(t_h) - u_h));
    e_h_vect = [e_h_vect; e_h];


end

legend('y_{ex}(t)', 'u_{h=0.5}', 'u_{h=0.25}', 'u_{h=0.125}', 'u_{h=0.0625}', 'u_{h=0.03125}')


u_f_vect
e_h_vect


figure

loglog(h_vect, e_h_vect)
grid on
hold on
loglog(h_vect, h_vect, '--k')
legend('e_{h}','h^{1}')
xlabel('h')
ylabel('err')