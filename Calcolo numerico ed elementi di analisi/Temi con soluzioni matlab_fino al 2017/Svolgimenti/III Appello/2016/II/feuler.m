function [t_h, u_h] = feuler(f,t0,tf,y0,h)

u_h = [y0];
t_h = [t0:h:tf];
u = y0;

n = (tf-t0)/h;


for k = 0:n-1
    t = t0 + k*h;
    u = u + h*f(t,u);
    
    u_h = [u_h, u];
end



return

