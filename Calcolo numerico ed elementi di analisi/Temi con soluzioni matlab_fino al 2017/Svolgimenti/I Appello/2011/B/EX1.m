close all
clear all

f = @(x) x.*cos(x);
a = -pi;
b = 2*pi;

x = [a:0.001:b];
plot(x,f(x))
grid on
hold on
h = area(x,f(x));
h.FaceColor = [0 0 1];
h.FaceAlpha = 0.25;
h.LineStyle = 'None';

%%

d2f = @(x) - 2*sin(x) - x.*cos(x);

tol = 1e-4;
M = ceil( sqrt( (((b-a)^3)/(12*tol)) * max(abs(d2f(x))) ) )

[ I ] = trapcomp( f, a, b, M )


%%

M_vect = 10*[1:10];
I_vect = [];

for i = 1:length(M_vect)
   M = M_vect(i);
   I = trapcomp( f, a, b, M );
   I_vect = [I_vect I];
end

I_ex = 2;
e_H_vect = abs(I_ex - I_vect);
H_vect = (b-a)./M_vect;

figure
loglog(H_vect, e_H_vect)
grid on
hold on
loglog(H_vect, H_vect.^2, '--k')

p_appr = log( e_H_vect(end-1)/e_H_vect(end) ) / log( H_vect(end-1)/H_vect(end) )






