f = @(x) exp(3*x).*(3*sin(x/3) + (1/3)*cos(x/3));
a = 0;
b = 1;

M_vect = [20 30 40];

I_t_c_vect = [];
e_t_c_vect = [];

for i = 1:length(M_vect)
    
    M = M_vect(i);
    
    x = linspace(a,b,M+1);
    H = (b-a)/M;

    I_t_c = 0;
    for k = 1+1:M+1
        I_t_c = I_t_c + (H/2)*(f(x(k-1)) + f(x(k)));
    end

    I_t_c_vect = [I_t_c_vect; I_t_c];

    
    
    I_ex = exp(3)*sin(1/3);
    e_n_f = abs(I_ex-I_t_c);
    
    e_t_c_vect = [e_t_c_vect; e_n_f];
    
end

I_t_c_vect
e_t_c_vect


H_vect = (b-a)./M_vect;


p_estim_1 = log(e_t_c_vect(1)/e_t_c_vect(2)) / log(H_vect(1)/H_vect(2))
p_estim_2 = log(e_t_c_vect(2)/e_t_c_vect(3)) / log(H_vect(2)/H_vect(3))

p_samples = [p_estim_1 p_estim_2];

p = round( sum(p_samples)/length(p_samples) )