%% 2 (Badass mode)
x = [0 1 2 4];
f = @(x) 4*x + 2;
y = f(x)
n = 3;

A = [(n+1)  sum(x);
    sum(x) sum(x.^2)]

q = [sum(y) sum(x.*y)]'
%alternatively
%q = [sum(y) dot(x,y)]'

a = A\q

figure
plot(x,y, 'o')
grid on
hold on
z = [min(x):0.1:max(x)];
r = @(x) a(1) + a(2)*x;
plot(z, r(z))


%% 2 (Pussy mode: what I'll actually do during the exam)
x = [0 1 2 4];
f = @(x) 4*x + 2;
y = f(x)
p = polyfit(x,y,1)
z = [min(x):0.1:max(x)];
pz = polyval(p,z);
figure
plot(z,pz)
grid on



%% 3
n = 5;
x = linspace(0,8,n+1)
f = @(x) 3*(1+sqrt(x));
pz = interp1(x,f(x),7.2)

%% 5
f = @(x) 3*x.^3;
df_appr_x0 = (f(3)-f(2.9))/.1


