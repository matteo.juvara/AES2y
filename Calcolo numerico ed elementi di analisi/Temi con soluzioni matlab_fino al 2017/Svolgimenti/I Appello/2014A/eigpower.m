function [ lambda, x, k ] = eigpower( A,tol,itermax,x0 )

k = 0;
x = x0;
y = x/norm(x);
lambda = y'*A*y
err = tol + 1;

while err > tol & k < itermax
    k = k+1;
    x = A*y;
    y = x/norm(x);
    lambda_old = lambda;
    lambda = y'*A*y;    
    err = abs(lambda - lambda_old)/abs(lambda);
    
end


end

