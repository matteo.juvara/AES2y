function [ lambdas, k ] = qrbasic( A, tol, itermax)

k = 0;

err = tol + 1;

while (err > tol) & (k < itermax)
    k = k+1;
    [Q,R] = qr(A);
    A = R*Q;
    lambdas = diag(A);
    
    err = max(max( abs( tril(A)-diag(lambdas) ) ));
end


end

