close all
clear all
a=0;
b=1;
x=[a:0.001:b];
f = @(x) x.* atan(cos(x));
plot(x,f(x))

format long e
M_vect = [5, 10, 20, 40, 80 ];
I_vect=[];
E_vect=[];

I_ex = 0.323393219617513;

for i=1:length(M_vect)
    M=M_vect(i);
    [ I ] = simpcomp(a, b, M, f)
    I_vect=[I_vect I];
    
    E = abs(I_ex-I);
    
    E_vect=[E_vect E];
end

loglog(M_vect, E_vect)
hold on
grid on
loglog(M_vect, M_vect.^(-4),'--k')
legend('errore','M^{-4}')