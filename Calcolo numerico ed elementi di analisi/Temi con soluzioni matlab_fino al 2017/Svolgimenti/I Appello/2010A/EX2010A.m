%% 1.1
n = 10;
A = diag([2:2:2*n]) + diag(sqrt([2:n]), -1) - diag(1./[2:n], 1);

x_exact = ones(n,1);
b = A*x_exact;

%% 1.2
% C.S. Esistenza ed Unicità fatt. LU:
% 1) A è SDP
% oppure
% 2) A è a dom. diag. stretta (righe o colonne)

% La 1) non è chiaramente verificata, per cui proviamo con la 2)


isStrictDiagDomRow = 1;

if det(A)~=0
    
    for i=1:n
        abs_a_ii = abs(A(i,i));
        sumAbsRow = sum(abs(A(i,:)))-abs_a_ii;
        if abs_a_ii <= sumAbsRow
            isStrictDiagDomRow = 0;
        end
    end

    
else
    error('Singular Matrix')
end

if isStrictDiagDomRow
    disp('YES Strict Diagonal Dominance Matrix')
else
    disp('NO Strict Diagonal Dominance Matrix')
end



%% 1.3
[L,U,P] = lu(A);

%Ly=b
y = fwsub(L,P*b);

%Ux=y
x = bksub(U,y)



%% 1.4
e_rel = norm(x-x_exact)/norm(x_exact)
r = b - A*x;
r_rel = norm(r)/norm(b)

K2_A = cond(A)

% OSS.: Essendo K_2(A) piccolo risulta che il residuo normalizzato
% stima efficacemente l'errore relativo 




%% 4.1
A = [3 1 -2; -1 24 10; 1 5 16];
gershcircles(A)
eig(A)
