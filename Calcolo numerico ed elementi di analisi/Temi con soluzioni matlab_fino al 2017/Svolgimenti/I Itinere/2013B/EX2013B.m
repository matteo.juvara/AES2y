close all

%% 1.1
f = @(x) x.^2.*(x-2).^5
alpha = 0;
beta = 2;
a = -0.2; b = 2.7;
x = [a:0.001:b];

plot(x, f(x))
grid on
axis equal

%% 1.4

% Case A
a = 1.5; b=2.3; tol1=1e-2; tol2=1e-5;
[ x_A, k_A ] = bisect( a,b,f,tol1,tol2 )
err_A = abs(x_A-beta)

% Case B
tol2=1e-15;
[ x_B, k_C ] = bisect( a,b,f,tol1,tol2 )
err_B = abs(x_B-beta)

% Case C
tol2=1e-20;
[ x_C, k_C ] = bisect( a,b,f,tol1,tol2 )
err_C = abs(x_C-beta)

% Case D
tol1=1e-6;
[ x_D, k_D ] = bisect( a,b,f,tol1,tol2 )
err_D = abs(x_D-beta)


%% 2.2
n = 10;
A = 10*eye(n) + diag(-2*ones(n-1,1), 1) + diag(-2*ones(n-1,1), -1) + diag(3*ones(n-2,1), 2) + diag(3*ones(n-2,1), -2) 



%% 2.3
B_J = eye(n)-inv(diag(diag(A)))*A;
rho_B_J = max(abs(eig(B_J)))

B_GS = eye(n)-inv(tril(A))*A;
rho_B_GS = max(abs(eig(B_GS)))

% Entrambi i raggi spettrali < 1 -> Sia J che GS convergono



%% 2.5
k_vect_J = [];
k_vect_GS = [];
n_vect = [10:10:50];
for n = n:10:50
    A = 10*eye(n) + diag(-2*ones(n-1,1), 1) + diag(-2*ones(n-1,1), -1) + diag(3*ones(n-2,1), 2) + diag(3*ones(n-2,1), -2) 
    b = [1:n]';
    x0 = ones(n,1);
    tol = 1e-5;
    itermax = 1000;
    
    
    [ ~, k_J ] = jacobi( A,b,x0,tol,itermax);
    k_vect_J = [k_vect_J k_J];

    [~, k_GS] = gs(A,b,x0,tol,itermax);
    k_vect_GS = [k_vect_GS k_GS];
end

figure
plot(n_vect, k_vect_J, 'b')
hold on
grid on
plot(n_vect, k_vect_GS, 'r')
xlabel('')