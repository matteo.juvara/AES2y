%% 1.3
f = @(x) (x-1).*cos(x);
df = @(x) cos(x) + (1-x)*sin(x);
a = 0;
b = pi;
x = [a:0.001:b];

plot(x,f(x))
grid on


%% 1.4
itermax = 100;

x0 = 1.1;
[x_vect,k1] = newton(x0,f,df,itermax);
xk_1 = x_vect(end)
k1

figure
subplot(1,2,1)
n = length(x_vect);
err1 = abs(x_vect - ones(n,1))
semilogy([1:n], err1);
grid on

stimap(x_vect)

%
x0 = 1.5;
[x_vect,k2] = newton(x0,f,df,itermax);
xk_2 = x_vect(end)
k2

subplot(1,2,2)
n = length(x_vect);
err2 = abs(x_vect - ones(n,1))
semilogy([1:n], err2);
grid on

stimap(x_vect)


% Dalla stimap vediamo che p=2 in entrambi i casi
% il che è in accordo con la teoria, essendo entrambi
% gli zeri semplici



%% 2.3
n = 10;
A = diag(3*ones(n,1)) + diag(-(sqrt(3)/2)*ones(n-1,1),1) + diag(-(sqrt(3)/2)*ones(n-1,1),-1) + diag(-0.5*ones(n-2,1),2) + diag(-0.5*ones(n-2,1),-2);

x_exact = [1:n]';
b = A*x_exact;

tol = 1e-6;
nmax = 100;
x0 = ones(n,1);
alpha = 1;

% Jacobi
P = diag(diag(A));
[ x, k ] = richardson( A, b, P, x0, tol, nmax, alpha )

% Gauss-Seidel
P = tril(A);
[ x, k ] = richardson( A, b, P, x0, tol, nmax, alpha )
