close all

%% 1.1
f = @(x) atan(x+1/10).*exp(-2*x);
a = -1; b = 1;
x = [a:.001:b];
plot(x,f(x))
grid on


%% 1.3
tol = 1e-6;
k_min = ceil(log2((b-a)/tol)-1)


%% 1.6
[x_vect,k] = bisez(a,b,tol,f)


%% 1.7
alpha_exact = -1/10;

err_vect = abs(x_vect-alpha_exact*ones(length(x_vect),1));
semilogy([1:k], err_vect)
grid on




%% 2.1
n = 10;
A = diag([1:2:2*n-1]) + diag(ones(n-1,1), 1) + 2*diag(ones(n-1,1), -1)
A(3:n,1) = [3:n]
b = [0:n-1]';

%% 2.2
[L,U,P] = lu(A);
P
% Scambio riga: 1 con 10

%% 2.3
%Ly=b
y = fwsub(L,P*b);

%Ux=y;
x = bksub(U,y);

x(10)


%% 2.5
D_inv = diag(1./diag(A));
B = eye(n)-D_inv*A;
rho_B = max(abs(eig(B)))