function [x_vect,k] = bisez(a,b,tol,f)

k = 0;
x0 = (a+b)/2;
x = x0;
x_vect = [];
err = tol + 1;

while err > tol
    
    k = k+1;
    
    if f(x)==0
        break
    end
    
    if f(a)*f(x) < 0
        b = x;
    elseif f(x)*f(b) < 0
        a = x;
    end
    
    x = (a+b)/2;
    x_vect = [x_vect; x];
    
    err = (b-a)/2;
    
end



return

