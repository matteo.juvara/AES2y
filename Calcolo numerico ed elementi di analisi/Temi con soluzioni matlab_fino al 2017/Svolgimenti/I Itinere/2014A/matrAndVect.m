function [ A, b ] = matrAndVect( n )

A  = zeros(n);
b = zeros(n,1)

for i = 1:n
    
    b(i) = 2^i;
    
   for j = 1:n
       
       if i==j
           A(i,j) = 4;
       elseif i==j-1
           A(i,j) = -1;
       elseif i==j+1
           A(i,j) = -1;
       else
           A(i,j)=0;
       end
       
   end
end

end

