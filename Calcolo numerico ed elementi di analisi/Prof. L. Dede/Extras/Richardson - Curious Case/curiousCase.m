close all

nMin = 2;
nMax = 160;

k_vect = [];
cond_vect = [];
n_vect = linspace(nMin,nMax,nMax-nMin+1);



A_fun = @(n) 4*eye(n) + diag(ones(n-1,1),1) + diag(ones(n-1,1),-1) + 2*diag(ones(n-2,1),2) + 2*diag(ones(n-2,1),-2);


for n = nMin:nMax
    
    A = A_fun(n);
    b = ones(n,1);

    itermax = 5000;
    x0 = zeros(n,1);
    tol = 1e-6;
    P = tril(A);

    [x, k] = richardson (A, b, P, x0, tol, itermax);
    
    k_vect = [k_vect; k];
    cond_vect = [cond_vect cond(A)];
    
    sprintf('Progress: %d/%d', n, nMax)
end

k_vect

set(0,'defaultTextInterpreter','latex')

figure('Name','Richardson Iterations Number for Large Sparse Matrices','NumberTitle','on');

subP(1) = subplot(2,2,1);
plot(n_vect, k_vect)
xlabel('$\mathbf{n}$ \quad (Square-Matrix Dimension)');
ylabel('$\mathbf{k}$ \quad (Iterations Numb.)');
grid on

subP(2) = subplot(2,2,2);
semilogy(n_vect, k_vect)
xlabel('$\mathbf{n}$ \quad (Square-Matrix Dimension)');
ylabel('$\mathbf{k}$ \quad (Iterations Numb.)');
grid on

subP(3) = subplot(2,2,3);
plot(linspace(nMin,nMax,nMax-nMin+1), cond_vect)
xlabel('$\mathbf{n}$ \quad (Square-Matrix Dimension)');
label_y = ylabel('$\mathbf{K_{2}(A)}$ \quad (2-Norm Cond. Numb.)');
grid on

subP(4) = subplot(2,2,4);
A = A_fun(20);
[x,y] = find(A);
clr = A(A~=0);
scatter(x,y,[],clr)
set(gca,'YDir','rev')
colorbar
grid on
title(subP(4),'\textbf{Matrix Pattern} for $\mathbf{A|_{n=20}}$')




