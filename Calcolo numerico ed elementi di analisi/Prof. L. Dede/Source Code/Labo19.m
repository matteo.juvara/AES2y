%% labo19
clear all
close all
clc

%% esercizio1
%domanda 1

L=1;
us=25;
ud=1000;

n=100;
h=L/(n+1);
xn=0:h:L;

q=0:4:12;

u=zeros(length(q),n);
dupper=-1*ones(n-1,1)/h^2;
dlower=dupper;

b=zeros(n,1);
b(1)=us/h^2;
b(end)=ud/h^2;


for r=1:length(q)
    d0=((2+q(r)*h^2)*ones(n,1))/h^2;
    [T,U,u(r,:)]=thomas3diag(dupper,dlower,d0,b);
end

figure(1)
uu=[us*ones(length(q),1),u,ud*ones(length(q),1)];
plot(xn,uu,'linewidth',2)
title('soluzione di u(x)al variare di q')
legend('q=0','q=4','q=8','q=12','q=16')


%domanda 2
f=2000*(1+sin(2*pi*xn(2:end-1)'./L));
b1=b+f;


for r=1:length(q)
    d0=((2+q(r)*h^2)*ones(n,1))/h^2;
    [T1,U1,u1(r,:)]=thomas3diag(dupper,dlower,d0,b1);
end

figure(2)
uu1=[us*ones(length(q),1),u1,ud*ones(length(q),1)];
plot(xn,uu1,'linewidth',2)
title('soluzione di u(x)al variare di q')
legend('q=0','q=4','q=8','q=12','q=16')