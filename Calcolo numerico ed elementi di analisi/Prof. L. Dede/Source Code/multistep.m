function [t,u] = multistep (a, b, tf, t0, u0, h, fun, dfun, toll, it_max)
%
%        [t,u] = multistep (a, b, tf, t0, u0, h, fun, dfun, toll, it_max)
%
%	 Metodo multistep per la risoluzione di EDO
%
%	 Parametri di ingresso:
%	 (N.B.: tutti i vettori di ingresso DEVONO essere colonna!)
%	   a           vettore dei coeff. a_j (dim. p+1)
%      b           vettore dei coeff. b_j (dim. p+2)
%	               (se implicito, b(1) contiene b_(-1)), altrimenti b(1)=0
%      t0	       vettore degli istanti iniziali (dim. p+1)
%	   tf          estremo finale intervallo temporale di integrazione
%	   u0          vettore dei dati iniziali (dim. p+1)
%	   h           passo di integrazione
%	   fun         function contenente f(t,y) (anonimous, o inline)
%
%    SOLO SE IMPLICITO: 
%      dfun        function contenente df/dy (anonimous o inline), 
%                  serve per la costruzione del metodo di Newton 
%	   toll        tolleranza metodo di Newton
%	   it_max      numero max di iterazioni di Newton
%
%	 Parametri di uscita:
%	   t, u        vettori contenenti le coordinate dei nodi
%		           di discretizzazione e i valori di u_n 
%   
t = t0; 
f = fun(t0,u0);
p = length(a) - 1;
u = u0;
nt = fix((tf - t0(1))/h);
for k = 1:(nt-p)
  lu = length(u);
  G = a' * u(lu:-1:lu-p)+ h * b(2:p+2)' * f(lu:-1:lu-p);
  lt = length(t0); 
  t0 = [t0; t0(lt)+h];
  t = t0(lt+1); 
% metodo implicito ==> Newton
  if (b(1) ~= 0),
    u_new = u(lu); 
    err = toll + 1; 
    it = 0;
    while (err >= toll) & (it <= it_max)
      y = u_new;
      den = 1 - h * b(1) * dfun(t,y);
      f_new = fun(t,y);
      if den == 0
        it = it_max + 1;
        fprintf('Annullamento del denominatore\n')
      else
        it = it + 1;
        u_new = u_new - (u_new - G - h * b(1) * f_new)/den;
        err = abs (u_new - y);
      end
    end
% metodo esplicito
  else  
    u_new = G;
    y = u_new;
    f_new = fun(t,y);
  end 
  u = [u; u_new]; f = [f; f_new];
end   
t= t0;
