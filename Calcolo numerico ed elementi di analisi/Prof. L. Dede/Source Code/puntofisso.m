function[succ,it]=puntofisso(x0,phi,nmax,toll)

err=toll+1;
it=0;
succ=x0;
xv=x0;

%corpo del ciclo
while(it<nmax && err<toll)
    xnew=phi(xv);
    err=abs(xnew-xv);
    succ=[succ;xnew];%cambia a ogni loop
    xv=xnew;
end

