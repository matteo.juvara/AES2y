%Labo15ese1
clear all
close all
clc


B=[10 -1 1 0
1 1 -1 3
2 0 2 -1
3 0 1 5];
%domanda 1
gershcircles(B)
gershcircles_mod(B)

%domanda 2
eig(B)

%domanda 3
[lambda1,x1,iter1]=eigpower(B);
[lambda,x,iter]=invpower(B);

%domanda 4
u=12;
[lambdainv,xinv,iterinv]=invpowershift(B,u)
u2=-i;
[lambdainv2,xinv2,iterinv2]=invpowershift(B,u2)


%% Ese2
N=4;
A=10* eye (N)+diag(ones(N-1,1),1)+diag(ones(N-1,1 ),-1);
W=qrbasic(A)

for i=1:length(W)-1;
    v1(i)=abs(W(i+1)/W(i));
end
V=max(v1)

