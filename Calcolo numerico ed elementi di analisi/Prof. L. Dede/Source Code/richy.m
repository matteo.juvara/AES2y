function [x,it]=richy(A,b,P,x0,toll,nmax,alfa)

x=x0;
it=0;
r=b-A*x;
err=norm(r)/norm(b);

while ((err>toll) && (it<nmax))
    z=P\r;
    if nargin==6
        alfa=(z'*r)/(z'*A*z);
    end
    x=x+alfa*z;
    r=b-A*x;
    err=norm(r)/norm(b);
    it=it+1;
end
it