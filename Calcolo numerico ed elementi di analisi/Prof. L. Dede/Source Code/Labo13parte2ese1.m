%labo 13 parte 2
clear all
close all
clc

%domanda 1
V=2.679664474685;

a=0;
b=0.8;
f=@(x)cosh(x-0.5);
x=linspace(a,b,1000);

y=f(x);
figure(1)
plot(x,y)
grid on

%domanda 2
N=100;
fun=@(x)pi*(cosh(x-0.5)).^2;
I1=pmedcomp(a,b,N,fun);%punto medio composito

%domanda 3
I2=trapcomp(a,b,N,fun);%trapezio composito

%domanda 4
I3=simpcomp(a,b,N,fun);%simpson composito

%domanda 5

N=[1:20];

V_PMC=zeros(1,N(end));
V_TC=zeros(1,N(end));
V_SC=zeros(1,N(end));

for i=N
    V_PMC(i)=pmedcomp(a,b,i,fun);
    V_TC(i)=trapcomp(a,b,i,fun);
    V_SC(i)=simpcomp(a,b,i,fun);
end

figure(2)
plot(N,V_PMC,N,V_TC,N,V_SC)
legend('puntomedio','trapezio','simpson')
grid on

%domanda 6
Err1=abs(V-V_PMC);
Err2=abs(V-V_TC);
Err3=abs(V-V_SC);

figure(3)
loglog(N,Err1,N,Err2,N,Err3)
legend('puntomedio','trapezio','simpson')
grid on

%domanda 7
toll=10.^-5;
f2=@(x)2.*pi.*(2.*(cosh(x-0.5)).^2-1);
f4=@(x)8.*pi.*(2.*(cosh(x-0.5)).^2-1);

K2=max(abs(f2(x)));
K4=max(abs(f4(x)));

N1=ceil((((b-a).^3)*K2./(24*toll)).^0.5);%intervalli punto medio
N2=ceil((((b-a).^3)*K2./(12*toll)).^0.5);%intervalli trapezio
N3=ceil((((b-a).^5)*K4./(16*180*toll)).^(1./4));%intervalli simpson

%domanda 8
I=gausscomp(a,b,N,fun);
