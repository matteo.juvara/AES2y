%sostituzione all'indietro
function[x]=backward(A,b)

x=[];
n=length(b);
%controllo la dimensione
if (size(A,1)~=n || size(A,2)~=n)
    error ('dimensioni non corrette');
end
%controllo che sia triangolare
if(A~=triu(A))
    error('matrice non triangolare superiore')
end
%controllo che la matrice non sia singolare
if(prod(diag(A))==0)
    error('matrice singolare')
end

%algoritmo
x=zeros(n,1);
x(n)=b(n)/A(n,n);
for i=n-1:-1:1
    x(i)=(b(i)-A(i,i+1:n)*x(i+1:n))/A(i,i);
end