%DATA STRUCT: structName.varName

% GEOMETRY
settings.d = 9e-2;
settings.S = pi*settings.d^2/4;
settings.ms = 8;
settings.mp = 2;
settings.tb = 2.1;
settings.mfr = settings.mp/settings.tb; %assumed to be constant
settings.m0 = settings.ms + settings.mp;
settings.Iyyf = 3.98;
settings.Iyye = 3.33;
settings.Idot = (settings.Iyyf - settings.Iyye) / settings.tb;

% THRUST
settings.T = 780;

% AERODYNAMICS
settings.CMa = -50;
settings.CD = 0.8;
settings.CLa = 3.222;

% WIND
settings.v_wind = [-1 0]';


% ODE
settings.time = [0:0.01:180];
settings.options = odeset('AbsTol', 1e-6, 'Events', @apogee);