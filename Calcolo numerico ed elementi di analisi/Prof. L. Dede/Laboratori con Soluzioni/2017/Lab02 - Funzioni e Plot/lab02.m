cubic = 'x.^3-1';
x = [0:3];
eval(cubic)

cubic = inline('x.^3-1', 'x');
cubic(x)

% we'll never use the above ones


cubic = @(x) x.^3-1;
x = linspace(-5,5,100);
y = cubic(x);

figure
plot(x,y)
grid on
hold on

f = @(x) 2.*x;
plot(x,f(x))
title('Functions')
legend('cubic','linear')


figure

x=[0:0.1:10];
semilogy(x,exp(x))
hold on;
semilogy(x,exp(2*x),'-*g')


figure


f = @(x) 2+(x-3).*sin(5.*(x-3));
x = [0:.001:6];

y = f(x);
grid on
hold on
plot(x,y,'k')

y1 = @(x) 5-x;
y2 = @(x) x-1;

plot(x, y1(x), 'r--')
plot(x, y2(x), 'b--')


figure
subplot(1,2,1)
f = @(x) (log(x)).^2;
x = linspace(.1,10,1000);
plot(x, f(x))
grid on


subplot(1,2,2)
semilogx(x, f(x)) % variable transform: x -> log(x)
grid on



%%%%%%%%
% OSS.: eps(x) changes each power of 2
[eps(1)
eps(2)
eps(4)
eps(5)
eps(8)
eps(15)]

figure
a = [0:.01:1]
b = linspace(0,1,101)
c = b-a;
plot([0:100], c, '*-')

x=1e-15
1-1+x
(1+x)-1
















