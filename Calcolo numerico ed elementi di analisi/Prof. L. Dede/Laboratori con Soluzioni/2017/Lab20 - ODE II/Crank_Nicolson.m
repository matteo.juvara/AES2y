function [t_h,u_h,k_fp_vect] = Crank_Nicolson(f,tf,y0,h)

t_h = [0:h:tf]';

u0 = y0;
u_h = [u0];
N_h = (tf-0)/h;

k_fp_vect = [];


itermax = 100;
tol = 1e-5;

for n = 1:N_h
    
    t_n = t_h(n);
    t_np1 = t_h(n+1);

    u_n = u_h(end);

    phi = @(u_np1) u_n +  (h/2)*(f(t_n, u_n) + f(t_np1, u_np1) );
    [u_np1, k_fp] = fixedPt(u_n, phi, itermax, tol);
    
    u_h = [u_h u_np1];
    k_fp_vect = [k_fp_vect k_fp];
    
end


end

