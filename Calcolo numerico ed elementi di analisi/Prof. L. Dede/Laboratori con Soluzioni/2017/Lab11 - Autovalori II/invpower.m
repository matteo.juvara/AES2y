function [mu,x,k]=invpower(A,tol,itermax,x0)



y = x0/norm(x0);

mu = y'*A*y;

err = tol + 1;

k = 0;

[L,U,P]=lu(A); % Calcolata una volta sola e riutilizzata

while (err > tol) && (k<itermax)
   k = k + 1;
   
   y1=fwsub(L,P*y);
   x=bksub(U,y1);
   y= x/norm(x);
   
   mu_old = mu;
   mu = y'*A*y;
   err = abs(mu - mu_old)/abs(mu);
end

eigV=sprintf('%d ', x);
sprintf('The eigenvalue with the lowest absolute value is: %d\nwhile the corresponding eigenvector is: %s', mu, eigV)




end

