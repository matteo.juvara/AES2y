function [ x_succ, k ] = conjgrad( A, b, x0, tol, itermax )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

r = b - A*x0;
p = r;

x = x0;
x_succ = [x0];

err = tol+1;
k = 0;



while (err > tol) && (k < itermax)
    
    k = k+1;
    
    alpha = (p'*r)/(p'*A*p);
    x = x + alpha*p;
    r = r - alpha*A*p;
    
    beta = (p'*A*r)/(p'*A*p);
    
    p = r - beta*p;
    
    
    x_succ = [x_succ x];
    
    err = norm(r)/norm(b);
    
end




end

