clc
clear

f=@(x) (x.^2+3)./(x-1);
x=linspace(-5,5,100);
plot(x,f(x))