function v = es13 (vettore)
n = length(vettore);
for i = 1:n
    [massimo, posizione] = max (vettore (i:n));
    temporaneo = vettore (i);
    vettore (i) = massimo;
    vettore (posizione + i - 1) = temporaneo;
end
v = vettore;