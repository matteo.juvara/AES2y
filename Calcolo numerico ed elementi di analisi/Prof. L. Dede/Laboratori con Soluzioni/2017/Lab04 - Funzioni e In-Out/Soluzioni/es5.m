function sqrtn = es5(n)
% Calcola la radice quadrata di n usando la
% successione di Erone.
r(1) = n;
toll = 1e-6;
incr = toll+1;
k = 2;
while incr > toll
    r(k) = 0.5*(r(k-1) + n/r(k-1));
    incr = abs(r(k) - r(k-1)); 
    k = k+1;
end
sqrtn = r(end);
plot(r,'ro-')
end
