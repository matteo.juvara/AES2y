function [ v_sort ] = sortDecr( v )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

n = numel(v);
v_sort = [0];

while length(v_sort)<=n
for i = 1:n
    
    if (v(i)==max(v))
        v_sort = [v_sort v(i)]
        v(i) = NaN;
    end
    
end

end

v_sort = v_sort(2:end)
end

