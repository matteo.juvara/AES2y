function fn = smd(t,y) %spring-mass-dumper

[n,m] = size(y);
fn = zeros(n,m);

gamma = 0.1;
omegaSq = 1;


fn(1) = y(2);
fn(2) = - omegaSq*y(1) - gamma*y(2) ;
    
    
    
end

