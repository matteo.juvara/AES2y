function [t_h, u_h] = feuler(f,t_f,y_0,h) %passo solo t_f poichè assumo di partire sempre da t_0 = 0;


t_h = [0:h:t_f];

u = y_0;

u_h = [u];

N_h = (t_f-0)/h;

for n = 1:N_h % OSS.: in MATLAB è tutto shiftato di 1 rispetto a teoria 0+1:N_h-1+1
    
    t_n = t_h(n);
    
    u = u +  h*f(t_n, u);

    u_h = [u_h u];
    
end

