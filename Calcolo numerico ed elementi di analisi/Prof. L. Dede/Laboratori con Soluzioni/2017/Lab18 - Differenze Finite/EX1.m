close all

%% 1.1-2-3
f = @(x) exp(-x.^2).*sin(2*x+1);
a = -3; b = 3;

df = @(x) 2*exp(-x.^2).*cos(2*x + 1) - 2*x.*exp(-x.^2).*sin(2*x + 1);

h_vect = 0.4*2.^-[0:5]

x0 = 0;

ffd_vect = []; % Forward Finite Difference
bfd_vect = []; % Backward Finite Difference
close all

cfd_vect = []; % Centered Finite Difference
    
for k = 1:length(h_vect)
    
    h = h_vect(k);
    ffd = (f(x0 + h) - f(x0))/h;
    bfd = (f(x0) - f(x0 - h))/h;
    cfd = (f(x0 + h) - f(x0 - h))/(2*h);

    ffd_vect = [ffd_vect ffd];
    bfd_vect = [bfd_vect bfd];
    cfd_vect = [cfd_vect cfd]
   
end


df_x0_ex = df(x0)

err_ffd_vect = abs(df_x0_ex - ffd_vect); 
err_bfd_vect = abs(df_x0_ex - bfd_vect);
err_cfd_vect = abs(df_x0_ex - cfd_vect);

figure
loglog(h_vect, err_ffd_vect)
hold on
grid on
loglog(h_vect, err_bfd_vect)
loglog(h_vect, h_vect,'--k')

loglog(h_vect, err_cfd_vect)
loglog(h_vect, h_vect.^2,'-.k')
set(0,'defaultTextInterpreter','latex')

legend('\delta_{+}f(x_{0})','\delta_{-}f(x_{0})','h^{1}','\delta_{c}f(x_{0})','h^{2}')
xlabel('h')
ylabel('error')











%% 1.4
clear all
close all

f = @(x) exp(-x.^2).*sin(2*x+1);
a = -3; b = 3;
h = 0.1;
x = [a+h:h:b-h];
df = @(x) 2*exp(-x.^2).*cos(2*x + 1) - 2*x.*exp(-x.^2).*sin(2*x + 1);



cfd_vect = [];


for x_i = a+h:h:b-h
    

    cfd = (f(x_i + h) - f(x_i - h))/(2*h);

    cfd_vect = [cfd_vect cfd];
   
end


figure
plot(x,df(x))
hold on
grid on
plot(x(1:end), cfd_vect);
legend('exact derivative', 'approx. derivative')


err_max = max(abs(df(x) - cfd_vect))



%% 1.6
f = @(x) exp(-x.^2).*sin(2*x+1);
a = -3; b = 3;

df = @(x) 2*exp(-x.^2).*cos(2*x + 1) - 2*x.*exp(-x.^2).*sin(2*x + 1);

h_vect = 0.4*2.^-[0:5]

x0 = 0;

ffd_vect = []; % Indeterminate Coefficients

    
for k = 1:length(h_vect)
    
    h = h_vect(k);
    a = 3/(2*h); b = -2/h; c = 1/(2*h);
    
    ffd = a*f(x0) + b*f(x0-h) + c*f(x0-2*h)

    ffd_vect = [ffd_vect ffd];
   
end


df_x0_ex = df(x0);

err_ffd_vect = abs(df_x0_ex - ffd_vect); 

figure
loglog(h_vect, err_ffd_vect)
hold on
grid on
loglog(h_vect, h_vect.^2,'--k')

set(0,'defaultTextInterpreter','latex')
legend('Indeterminate Coefficients', 'h^{2}')

xlabel('h')
ylabel('error')



