v1 = 2.^(0:10)

v2 = (cos(pi./(1:10)))'

v3 = .1./(2.^(0:5))

v4 (1:2:19) = exp(1:10) + (-1).^(0:9).*(5.*(1:10) + 1)


A = zeros(5) + 2*diag(ones(5,1)) + 10*diag(ones(3,1),2) + 10*diag(ones(3,1),-2) + 5*diag(ones(4,1),-1) + 40*diag(1,4) + 40*diag(1,-4)

sumAllElems = sum(sum(A,1)' + sum(A,2))

A1 = A(1:3, 1:3)
A2 = A(1:2:5, [1 2 4])
A3 = A(2:4, [1 3 4])

onesMatr = eye(10)
B = ones(10)-onesMatr(:,end:-1:1)

n = 200
C = zeros(n) + diag([1:n]) + diag(ones(n-1,1),1) + diag(ones(n-1,1),-1) + .5*diag(ones(n-2,1),2) + .5*diag(ones(n-2,1),-2)

D = zeros(10) + diag([20:-2:2]) + diag(3.^(0:8),1) + .5*diag(ones(8,1),-2)