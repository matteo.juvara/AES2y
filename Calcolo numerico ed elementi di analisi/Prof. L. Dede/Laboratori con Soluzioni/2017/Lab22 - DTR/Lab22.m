close all

f = @(x) exp(3*x).*(-4 + 3*x + 9*x.^2);

x0 = 0; xf = 1;
alpha = 1; beta = 1;

u_ex = @(x) exp(3*x).*(x-x.^2) + 1;

%% 1
N = 19; % con (N+1) sottointervalli, (N+2) Nodi
h = (xf-x0)/(N+1);
knots = [x0:h:xf];

A = 1/(h^2) * (diag(-ones(N-1,1), -1) + diag(2*ones(N,1), 0) + diag(-ones(N-1,1), 1));

b_bound = zeros(N,1);
b_bound(1) = alpha/(h^2);
b_bound(end) = beta/(h^2);
b = f(knots(2:end-1)') + b_bound;

u = A \ b;
u = [alpha; u; beta];

err = abs( u - u_ex(knots)');


N_vect = [10:10:150] - 1;
h_vect = (xf-x0)./(N_vect+1);

err_vect = [];
for k = 1:length(h_vect)
    
    h = h_vect(k);
    N = N_vect(k);
    
    knots = [x0:h:xf];

    A = 1/(h^2) * (diag(-ones(N-1,1), -1) + diag(2*ones(N,1), 0) + diag(-ones(N-1,1), 1));

    b_bound = zeros(N,1);
    b_bound(1) = alpha/(h^2);
    b_bound(end) = beta/(h^2);
    b = f(knots(2:end-1)') + b_bound;
    
    u = A \ b;
    u = [alpha; u; beta];

    err = max(abs( u - u_ex(knots)'));
    err_vect = [err_vect err];
end

figure
loglog(h_vect, err_vect,'bo-')
grid on
hold on
loglog(h_vect,h_vect.^2','k--')

legend('e_{h}', 'h^{2}')
xlabel('h')
ylabel('e_{h}')



% per 151 nodi
figure
plot(knots, u, 'o')
grid on
hold on
plot(knots, u_ex(knots))

legend('u','u_{ex}')



%% 2
x0 = 0; xf = 1;
alpha = 0; beta = exp(1)*sin(5);

f = @(x) exp(x) .* (24*sin(5*x) - 10*cos(5*x) + (sin(5*x)).^2);

q = @(x) sin(5*x); % funzione di reazione ( cioè sigma=sigma(x) )

u_ex = @(x) exp(x).*sin(5*x);



h_vect = 0.1*2.^-[0:5];
N_vect = ((xf-x0)./h_vect) - 1;

err_vect = [];
for k = 1:length(h_vect)
    
    h = h_vect(k);
    N = N_vect(k);
    
    knots = [x0:h:xf];

    A = 1/(h^2) * (diag(-ones(N-1,1), -1) + diag(2*ones(N,1), 0) + diag(-ones(N-1,1), 1)) + diag(q(knots(2:end-1)));

    b_bound = zeros(N,1);
    b_bound(1) = alpha/(h^2);
    b_bound(end) = beta/(h^2);
    b = f(knots(2:end-1)') + b_bound;
    
    u = A \ b;
    u = [alpha; u; beta];

    err = max(abs( u - u_ex(knots)'));
    err_vect = [err_vect err];
    
   
    if k == 1
        figure
        plot(knots, u, '-o')
        grid on
        hold on
        x_axis = [x0:0.001:xf];
        plot(x_axis, u_ex(x_axis))

        legend('u','u_{ex}')
    end
    
end


figure
loglog(h_vect, err_vect,'bo-')
grid on
hold on
loglog(h_vect,h_vect.^2','k--')

legend('e_{h}', 'h^{2}')
xlabel('h')
ylabel('e_{h}')






