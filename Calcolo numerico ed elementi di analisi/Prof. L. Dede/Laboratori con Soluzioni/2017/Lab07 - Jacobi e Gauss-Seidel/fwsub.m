function y =fwsub(L,b)

% function [x] = fwsub(A,b)
% Algoritmo di sostituzione in avanti
% A: matrice quadrata triangolare inferiore
% b: termine noto
% x: soluzione del sistema Ax=b

n=length(b);

if ((size(L,1)~=n)||(size(L,2)~=n))
  error('ERRORE: dimensioni incompatibili')
end

if ~isequal(L,tril(L))
  error('ERRORE: matrice non triangolare inferiore')
end


if (prod(diag(L))==0)
% almeno un elemento diagonale nullo
  error('ERRORE: matrice singolare')
end

y=zeros(n,1);

y(1)=b(1)/L(1,1);

for i=2:n
  y(i)=(b(i)-L(i,1:i-1)*y(1:i-1))/L(i,i);
end
