close all
clear all

make_plot=1;


aa=0;
bb=1;
T=1;

f= @(x,t) (-sin(t)+0.25*cos(t))*sin(x'/2)  ; % questo restituisce un vettore ! 
u_esatta= @(x,t) sin(x./2)'*cos(t);

us=@(t) u_esatta(0,t);
ud=@(t) u_esatta(1,t);
u0=@(x) u_esatta(x,0);


%% PUNTO 1: Eulero implicito

theta=1;

n=100; % nodi interni
deltat=0.1;

[U, griglia_x,passi_t] = diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot);


%% PUNTO 2: Eulero esplicito

theta=0;

[U, griglia_x,passi_t] = diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot);

n=10;
deltat=0.001;

[U, griglia_x,passi_t] = diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot);

%% PUNTO 3
n=10;
deltat_CN=0.2./(2.^[1:6]);
deltat_EE=0.002./(2.^[1:6]);

make_plot=0;

for i=1:6
    
    % Crank-Nicholson
    deltat=deltat_CN(i);
    theta=1/2;
    [U,griglia_x,passi_t] = diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot);
    u_CN=U(:,end);
    u_ex=u_esatta(griglia_x,passi_t(end));
    errore_CN(i)=max(abs(u_ex-u_CN));
    
    % Eulero Implicito
    deltat=deltat_CN(i);
    theta=1;
    [U,griglia_x,passi_t] = diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot);
    u_EI=U(:,end);
    u_ex=u_esatta(griglia_x,passi_t(end));
    errore_EI(i)=max(abs(u_ex-u_EI));
    
    % Eulero Esplicito
    deltat=deltat_EE(i);
    theta=0;
    [U,griglia_x,passi_t] = diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot);
    u_EE=U(:,end);
    u_ex=u_esatta(griglia_x,passi_t(end));
    errore_EE(i)=max(abs(u_ex-u_EE));
    
end

figure
loglog(deltat_CN,errore_CN,'-ok','LineWidth',2);
hold on
loglog(deltat_CN,errore_EI,'-oc','LineWidth',2);
loglog(deltat_CN,1/5000*deltat_CN,'--','LineWidth',2);
loglog(deltat_CN,1/500*deltat_CN.^2,'--r','LineWidth',2)

xlabel('\Deltat')
ylabel('errore')
title('convergenza dell''errore di approssimazione','fontsize',16)
legend('errore_{CN}','errore_{EI}','\Deltat','\Deltat^2',2)
    
    
figure
loglog(deltat_EE,errore_EE,'-og','LineWidth',2);
hold on
loglog(deltat_EE,1/5000*deltat_EE,'--','LineWidth',2);
loglog(deltat_EE,1/500*deltat_EE.^2,'--r','LineWidth',2)

xlabel('\Deltat')
ylabel('errore')
title('convergenza dell''errore di approssimazione','fontsize',16)
legend('errore_{EE}','\Deltat','\Deltat^2',2)
    
    