clc; clear all; close all
%Es1 punto 1 (teorico)

%Es1 punto 2
x0 = 0;
xL = 2*pi;
h = 2*pi/30;
xj=[x0:h:xL];
xdof = [x0+h:h:xL-h]';
Nh = length(xdof);
% N_sc: numero di sottointervalli per simpcomp (N=1 integrazione semplice)
% per trovare il termine noto
N_sc=1; 
% matrice
epsilon = 10;    % diffusione

U  = ones(Nh,1);
U1 = ones(Nh-1,1);

A = epsilon/h  * ( 2*diag(U) - diag(U1,1) - diag(U1,-1) );
% termine noto
frhs = @(x) -20.*exp(x).*cos(x);

fh=zeros(size(xdof));
for j=2:Nh+1
    f1=@(x) frhs(x).*(x-xj(j-1))/h;
    f2=@(x) frhs(x).*(-x+xj(j+1))/h;
    fh(j-1)=simpcomp(xj(j-1),xj(j),N_sc,f1)+simpcomp(xj(j),xj(j+1),N_sc,f2);
end
% in alternativa, per la definizione del termine noto, si può usare 
% la formula dei trapezi sui sottointervalli e si ottiene: 
% fh = h*frhs(xdof); % -> errore più elevato

%Es1 punto 3
if min(eig(A))>0
    fprintf('definita positiva')
else 
    fprintf('non definita positiva')
end

%Es1 punto 4, condizionamento di A
cond(A)

%Es1 punto 5 e 6
% risoluzione sistema
uh = A\fh;

%aggiunta dei bordi
x_bc = [x0;xdof;xL];
u_bc = [0;uh;0];
plot(x_bc,u_bc,'o-'); hold on

% soluzione esatta
uex = @(x) exp(x).*sin(x);
uex_x = @(x) exp(x).*(sin(x)+cos(x));
xdis = linspace(x0,xL,1000);
plot(xdis,uex(xdis),'k')
legend('Soluzione approssimata','Soluzione esatta')
%Es1 punto 7
%calcolo dell'errore in norma L2:
integrandaL2 = @(x) (uex(x) - interp1(x_bc,u_bc,x)).^2;
errL2 = sqrt(simpcomp(x0,xL,1000,integrandaL2))

%Es1 punto 8,9,10

NN = 10*2.^(1:7);
H = 2*pi./NN;
K = [];
E = [];
E1 = [];
for h = H;
    
    xj=[x0:h:xL]';
    xdof = [x0+h:h:xL-h]';
    Nh = length(xdof);

    U = ones(Nh,1);
    U1 = ones(Nh-1,1);

    A = epsilon/h  * ( 2*diag(U) - diag(U1,1) - diag(U1,-1) );
    
    K = [K cond(A)];
    fh=zeros(size(xdof));
    for j=2:Nh+1
        f1=@(x) frhs(x).*(x-xj(j-1))/h;
        f2=@(x) frhs(x).*(-x+xj(j+1))/h;
        fh(j-1)=simpcomp(xj(j-1),xj(j),N_sc,f1)+simpcomp(xj(j),xj(j+1),N_sc,f2);
    end
    % in alternativa, per la definizione del termine noto, si può usare 
    % la formula dei trapezi sui sottointervalli e si ottiene: 
    % fh = h*frhs(xdof); % -> errore più elevato


    uh = A\fh;
    
    x_bc = [x0;xdof;xL];
    u_bc = [0;uh;0];

    integrandaL2 = @(x) (uex(x) - interp1(x_bc,u_bc,x)).^2;
    errL2 = sqrt(simpcomp(x0,xL,1000,integrandaL2));
    E = [E errL2 ]; 
    
    % e se si volesse calcolare la norma H1 della soluzione: Va
    % approssimata la derivata prima della soluzione numerica in tutti i
    % nodi.. oppure si considerano dei nodi intermedi, 
    % a cui si aggiungono gli estremi..
    
    % primo modo
%     integrandaH1 = @(x) integrandaL2(x)+...
%         (uex_x(x)-interp1(x_bc,...
%         [u_bc(2)-u_bc(1);(u_bc(3:end)-u_bc(1:end-2))/2;u_bc(end)-u_bc(end-1)]/h,x)).^2;
    
    % secondo modo
    x_inter=[x_bc(1);(x_bc(2:end)+x_bc(1:end-1))/2;x_bc(end)];
    
    integrandaH1 = @(x) integrandaL2(x)+...
        (uex_x(x)-interp1(x_inter,...
        [u_bc(2)-u_bc(1);u_bc(2:end)-u_bc(1:end-1);u_bc(end)-u_bc(end-1)]/h,x)).^2;
    
    
    errH1 = sqrt(simpcomp(x0,xL,1000,integrandaH1));
    E1 = [E1 errH1];

end

figure(2);
% l'errore in norma L2 va come h^2 per h->0, come ci si aspetta dalle stime
% teoriche dell'errore
loglog(H,E,'bo-',H,E1,'go-',H,H,'k',H,H.^2,'k--')
legend('errL2','errH1','H','H^2')
figure(3)
% il numero di condizionamento della matrice va come h^-2, come ci si
% aspetta dalla teoria
loglog(H,K,'ro-',H,H.^-2,'k')
legend('cond(A)','H^{-2}')

