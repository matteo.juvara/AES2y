%% CALCOLO NUMERICO ED ELEMENTI DI ANALISI A.A. 2013-2014 Aerospaziale
%   Prof. Antonietti Paola, Esercitatori: Brugiapaglia S., Signorini M.
%                  SOLUZIONI LABORATORIO 2 


%% ESERCIZIO 1
x=[0:3];
f1=@(x) x.*sin(x)+(1/2).^(sqrt(x));
f1(x)
% oppure, alternativamente,

fprintf(['f1(x)= [',num2str(f1(x)),']\n']);

% fprintf('stringa') stampa a video la stringa che gli viene passata tra
% apici
% posso concatenare diverse stringhe tra di loro, come se fosse un vettore
% riga: stringa=['stringa1','stringa2','stringa3'];
% num2str(valore numerico) trasforma un valore numerico in stringa.
% \n è un carattere speciale per andare a capo

% PER OGNI DUBBIO, L'HELP di MATLAB vi AIUTA! se avete ancora dubbi,
% chiedete!

f2=@(x) x.^4+log(x.^3+1);
f2(x)

% oppure, alternativamente,

fprintf(['f2(x)= [',num2str(f2(x)),']\n']);

%% ESERCIZIO 2
x=[0:0.01:6];
f=@(x) 2+(x-3).*sin(5*(x-3));
% le rette che delimitano la funzione si ottengono prendendo il valore
% massimo e minimo del seno che sono +1 e -1.
r1=@(x) -x+5;
r2=@(x) x-1;
plot(x,f(x),'k');
hold on
plot(x,r1(x),'--r',x,r2(x),'--r');
title('ES2');
% il comando plot consente di disegnare più linee contemporaneamente, senza
% dover utilizzare il comando hold on, nel
% seguente modo: plot(x1,y1,opt1,x2,y2,opt2). Ad ogni coppia di (x,y) che
% voglio disegnare sullo stesso grafico, devo dare il tipo di linea e
% opzioni. Se non vengono date opzioni, matlab disegna ogni linea con un
% colore diverso.

% HELP MATLAB per vedere che opzioni ha il comando LEGEND e TITLE

figure;
plot(x,f(x),'k',x,r1(x),'--r',x,r2(x),'--r');
title('ES2 - un unico comando con opzioni linee e colore');
legend('y=2+(x-3)sin(5(x-3))','limiti','location','North')
figure;
plot(x,f(x),x,r1(x),x,r2(x));
title('ES2 - un unico comando senza opzioni linee e colore');
legend('y=2+(x-3)sin(5(x-3))','limite 1: y=5-x','limite 2: y=x-1',...
    'location','SouthOutside')
%% ESERCIZIO 3

figure;
x=[0.1:0.01:10];
logf=@(x)(log(x)).^2;
semilogx(x,logf(x)) 
title('grafico in scala semilogaritmica  -  semilogx')
legend('y=log(x)^2')
% NOTA: stiamo disegnando un grafico in scala logaritmica sull'asse x, 
% ma non sull'asse y. Il grafico risultante è l'insieme dei punti di
% coordinate (log(x),log(x)^2), quindi ne risulta una parabola.
