% Laboratorio 4 Esercizio 3
% Scrittura su file con formato definito dall'utente
% Script chhe realizza una tabella di n valori di coseno e seno
% tra 0 e pigreco

n = input('Inserisci il numero di valori : ');
x = linspace(0,pi,n);
c = cos(x);
s = sin(x);
f1 = fopen('es3.out', 'w');
fprintf(f1, '------------------------------------------\n');
fprintf(f1, 'k\t x(k)\t cos(x(k))\t sin(x(k))\n');
fprintf(f1, '------------------------------------------\n');
fprintf(f1, '%2d\t %.3f\t %.8f\t %.8f\n', [1:n;x;c;s]);
fclose(f1);
