function [ determinante ] = det23bis(A,i)

% function [ determinante ] = det23bis( A,i )
% Laboratorio 4 Esercizio 1: scrivere una funzione che calcoli il
% determinante di matrici quadrate di ordine 2 e 3
%
% i e' la riga che si vuole usare per il calcolo
% del determinante, per matrici 2x2 puo assumere
% solo valori 1 e 2. Per matrci 3x3 puo assumere
% anche valore 3.


if (nargin == 1)
  i=1;
end
determinante =0;
[n, m] = size(A);

if (i > n)
      error('la riga scelta non esiste')
end

if ( n==m )  
  if ( n==2)
      determinante = A(1,1)*A(2,2)-A(2,1)*A(1,2);
  elseif ( n==3 )
      for j = [1:n]
          determinante = determinante + (-1)^(i+j) * A(i,j) * ...
                         det2( A([1:i-1,i+1:n],[1:j-1,j+1:n] ));
      end
  else
      error('matrice di dimensioni non accettabili')
  end
end

return
