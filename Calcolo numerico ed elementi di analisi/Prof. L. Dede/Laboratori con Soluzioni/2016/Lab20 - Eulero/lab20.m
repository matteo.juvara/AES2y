salva=0;


% Pb Cauchy:
% y'= lambda y 
% y(0) = b
%
% soluzione esatta: b exp(lambda*t), con c=lambda*b

y0=0.1;
lambda=2.4;

tmax=3;

t_plot=0:0.01:tmax;

y=@(x) y0*exp(lambda*x);

plot(t_plot,y(t_plot));

title('Soluzione analitica','FontSize',20)

%% risolvo con eulero in avanti

% t_h è l'insieme dei nodi in cui e' calcolata la soluzione, u_h e' la soluzione discreta

% questo e' il termine di destra della EDO

f= @(t,y) lambda*y;

h=0.05;

[EA_t_h,EA_u_h]=eulero_avanti(f,tmax,y0,h);


% confronto il risultato con la soluzione esatta

figure;
plot(t_plot,y(t_plot),'k','LineWidth',2);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);

hold on;

plot(EA_t_h,EA_u_h,'o','MarkerSize',4);

%title('soluzione approssimata con Eulero avanti','FontSize',20)

leg=legend('soluzione analitica','soluzione calcolata con EA, h=0.05',2);

set(leg,'FontSize',16)

%salva=1;

if (salva==1), saveas(gcf,'EA.eps','epsc2'), end


%% confronto con un passo piu' fine

% raffino h

h=0.01;

[EA_t_h_fine,EA_u_h_fine]=eulero_avanti(f,tmax,y0,h);

figure;
plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on

plot(EA_t_h,EA_u_h,'o','MarkerSize',4);

plot(EA_t_h_fine,EA_u_h_fine,'or','MarkerSize',4);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);


%title('confronto diversi passi h','FontSize',16)

leg=legend('soluzione analitica','EA con h=0.05','EA con h=0.01',2);

set(leg,'FontSize',16)

if (salva==1), saveas(gcf,'EA_confronto_h.eps','epsc2'), end


%% risolvo con eulero indietro


% la sintassi della chiamata e' la stessa di prima

h=0.05;

[EI_t_h,EI_u_h,iter_pf]=eulero_indietro(f,tmax,y0,h);



% confronto il risultato con la soluzione esatta

figure;
plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on;

plot(EI_t_h,EI_u_h,'o','MarkerSize',4);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);



%title('soluzione approssimata con Eulero indietro','FontSize',20)

leg=legend('soluzione analitica','soluzione calcolata con EI, h=0.05',2);

set(leg,'FontSize',16)

if (salva==1), saveas(gcf,'EI.eps','epsc2'), end


% visualizzo le sottoiterazioni di punto fisso ad ogni iterazione 
figure;

plot(EI_t_h,iter_pf,'o:','MarkerSize',4)

xlabel('istante','FontSize',16);
ylabel('iterazioni di punto fisso','FontSize',16);


%title('sottoiterazioni di punto fisso','FontSize',16)

if (salva==1), saveas(gcf,'iterazioni_pf.eps','epsc2'), end



%% confronto con un passo piu' fine

% raffino h

h=0.01;

[EI_t_h_fine,EI_u_h_fine]=eulero_indietro(f,tmax,y0,h);

figure;
plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on

plot(EI_t_h,EI_u_h,'o','MarkerSize',4);

plot(EI_t_h_fine,EI_u_h_fine,'or','MarkerSize',4);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);


%title('confronto diversi passi h','FontSize',16)

leg=legend('soluzione analitica','EI con h=0.05','EI con h=0.01',2);

set(leg,'FontSize',16)

if (salva==1), saveas(gcf,'EI_confronto_h.eps','epsc2'), end


%% grafici errore

%passi=0.05:-0.01:0.01;
N=5;
dimezz=2.^(1:N);
passi=0.08./dimezz;


for it=1:N
      [EA_t_h,EA_u_h]=eulero_avanti( f, tmax, y0, passi(it) );
      [EI_t_h,EI_u_h]=eulero_indietro( f, tmax, y0, passi(it) );
      y_h=y( 0 : passi(it) : tmax  );
      errore_EA(it)=max( abs (y_h-EA_u_h) );
      errore_EI(it)=max( abs (y_h-EI_u_h) );
end


% figure;
% 
% plot( 0:passi(it):tmax, y(0:passi(it):tmax), 'k')
% hold on
% plot(EA_t_h,EA_u_h,'-ob');
% plot(EI_t_h,EI_u_h,'-or');



%figure;

% questo servira' dopo, e' un trucco per visualizzare meglio l'errore, separando le linee che indicano la pendenza della convrgenza dai
% risultati
%m=( errore_EA(1)-errore_EA(end) ) / (passi(1) -passi(end));

% plot(passi,errore_EA,'-ob','LineWidth',2);
% hold on;
% plot(passi,errore_EI,'-or','LineWidth',2);
% plot(passi,m*passi,'k')
%plot(passi,(m*passi).^2,'k:')


% figure;
% 
% semilogy(passi,errore_EA,'-ob','LineWidth',2);
% hold on
% semilogy(passi,errore_EI,'-or','LineWidth',2);
% plot(passi,passi,'k')
% plot(passi,passi.^2,'k:')

figure;

loglog(passi,errore_EA,'-ob','LineWidth',2);
hold on
loglog(passi,errore_EI,'-or','LineWidth',2);

%questi plot utilizzano implicitamente le scale logaritmiche utilizzate qui sopra

plot(passi,100*passi,'k')
plot(passi,(10*passi).^2,'k:')

xlabel('h','FontSize',16);
lab=ylabel('err(h)','FontSize',16,'Rotation',0);
set(lab,'Position',[0.0007 0.2])

leg=legend('errore EA','errore EI','ordine 1','ordine 2',4);

set(leg,'FontSize',16)


if (salva==1), saveas(gcf,'andamento_errori.eps','epsc2'), end







