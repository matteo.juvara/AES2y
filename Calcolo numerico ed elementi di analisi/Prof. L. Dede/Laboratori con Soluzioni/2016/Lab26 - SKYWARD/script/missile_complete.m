function [ dY ] = missile_complete( t,Y,settings )
% The MIT License (MIT)
% 
% Copyright (c) 2014,Skyward Experimental Rocketry
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

%Useful Renamings

%x = Y(1);
z = Y(2);
theta = Y(3);
u = Y(4);
w = Y(5);
q = Y(6);
m = Y(7);
Iy = Y(8);

% Loading Aero coefficients for full and empty configuration (these are
% number of Alpas x number of Betas x number of Altitudes TABLES use
% size() to check the dimensions)

CDfull = settings.Aerofull.CD;
CLfull = settings.Aerofull.CL;
CMfull = settings.Aerofull.CM;
CMAfull = settings.Aerofull.CMA;

CDempty = settings.Aeroempty.CD;
CLempty = settings.Aeroempty.CL;
CMempty = settings.Aeroempty.CM;
CMAempty = settings.Aeroempty.CMA;


S = settings.S;
C = settings.C;
tb = settings.tb;
g = 9.81;

%Wind
vwind=[(settings.vwind0(1)*(abs(z)/abs(settings.z0))^settings.n), settings.vwind0(2)];
%vwind =[settings.vwind0(1);settings.vwind0(2)];


% Rotation matrixes
Rib = [
    cos(theta) sin(theta)
    sin(theta) -cos(theta)]; %Inertial to body

V = [u-vwind(1) w-vwind(2)]'; %Adding wind effects
Vb = Rib*V;

alpha = atan(Vb(2)/Vb(1));

% The second condition is to not go outside the interpolation boundaries
if (abs(V(1)) <1E-4)
    alpha = 0;
end

% The data set is given on limited alpha so I have to restrict the range to
% not make wrong extrapolation

if alpha*180/pi > max(settings.Aerofull.Alphas)
    alpha = max(settings.Aerofull.Alphas)*pi/180;
elseif alpha*180/pi < min(settings.Aerofull.Alphas)
    alpha = min(settings.Aerofull.Alphas)*pi/180;
end

Rbw = [
    cos(alpha) sin(alpha)
    -sin(alpha) cos(alpha)]; %Body to wind

if z<0
    z=0;
end

[~,a,~,rho] = atmoscoesa(z);

%Calculating current Mach
Mach = norm(V)/a;

% For interpolation angles are in degree!

[givA,givM,givAlts]=ndgrid(settings.Aerofull.Alphas,...
    settings.Aerofull.Machs,settings.Aerofull.Alts);
[curA,curM,curAlt] = ndgrid(alpha*180/pi,Mach,z);

%Multi-variable interpolation (with 'spline' option interpn extrapolates
%also outside the boundaries of the data set);

CDf=interpn(givA,givM,givAlts,CDfull,curA,curM,curAlt,'spline');
CLf=interpn(givA,givM,givAlts,CLfull,curA,curM,curAlt,'spline');
CMf=interpn(givA,givM,givAlts,CMfull,curA,curM,curAlt,'spline');
CMAf=interpn(givA,givM,givAlts,CMAfull,curA,curM,curAlt,'spline');

CDe=interpn(givA,givM,givAlts,CDempty,curA,curM,curAlt,'spline');
CLe=interpn(givA,givM,givAlts,CLempty,curA,curM,curAlt,'spline');
CMe=interpn(givA,givM,givAlts,CMempty,curA,curM,curAlt,'spline');
CMAe=interpn(givA,givM,givAlts,CMAempty,curA,curM,curAlt,'spline');


%Linear variation from full to empty configuration
if t<settings.tb
    CD = t/tb*(CDe-CDf)+CDf;
    CL = t/tb*(CLe-CLf)+CLf;
    CM = t/tb*(CMe-CMf)+CMf;
    CMA = t/tb*(CMAe-CMAf)+CMAf;
else
    CD = CDe;
    CL = CLe;
    CM = CMe;
    CMA = CMAe;
end


D = 0.5*rho*norm(V).^2*S*CD;
L = 0.5*rho*norm(V).^2*S*CL;
M = 0.5*rho*norm(V).^2*S*C*(CM+CMA*alpha);

Fb = Rbw'*[-D -L]';

%pause
if t<tb
    % Interpolating thrust
    T = polyval(settings.T,t);
    Idot = settings.Idot;
    mfr = settings.mfr;
else
    T = 0;    
    Idot = 0;
    mfr = 0;
end

% Adding thrust
Fb = Fb + [T;0];

F = Rib'*Fb;

F = F + [0;-m*g];

dY(1) = u;
dY(2) = w;
dY(3) = q;
dY(4) = F(1)/m+mfr*u/m;
dY(5) = F(2)/m+mfr*w/m;
dY(6) = M/Iy+Idot*q/Iy;
dY(7) = -mfr;
dY(8) = -Idot;

dY = dY';


end

