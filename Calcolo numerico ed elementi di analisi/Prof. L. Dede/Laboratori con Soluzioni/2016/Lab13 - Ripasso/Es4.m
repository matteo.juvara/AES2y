addpath ../Fornite
%%% Es4 %%%
close all
clear all
% definizione del dominio e della funzione
a=-pi/4;
b=pi;
f=@(x)sin(x).*cos(x);

% definizione dell'errore commesso nel caso di interpolazione polinomio
% Lagrange con nodi equispaziati e nel caso di interpolazione lineare
% composita
ErrLag=@(n) 2.^n./(4*(n+1)).*((b-a)./n).^(n+1);
ErrInt1=@(n) ((b-a)./n).^2/4;
N=1:12;

figure(1);
semilogy(N,ErrLag(N))
title('Errore interpolazione polinomio Lagrange');
xlabel('n');
ylabel('||f-\Pi_n||_{\infty}')
hold on

figure(2);
semilogy(N,ErrInt1(N))
hold on
title('Errore interpolazione lineare composita');
xlabel('n');
ylabel('||f-\Pi_1||_{\infty}')

% vettore per valutare le funzioni nel dominio [a,b]
x=linspace(a,b,100);
% polinomio interpolatore di Lagrange e interpolazione composita lineare
% con n+1 nodi
for n=N
    % nodi di interpolazione
    X0=linspace(a,b,n+1);
    % Polinomio interpolatore di Lagrange 
    P=polyfit(X0,f(X0),n);
    figure(3)
    plot(x,polyval(P,x));
    hold on
    plot(X0,f(X0),'go');
    pause(0.5) 
    % calcolo degli errori commessi in norma infinito
    errLag(n)=norm(f(x)-polyval(P,x),inf);
    % Interpolazione composita lineare
    y=interp1(X0,f(X0),x);
    errInt1(n)=norm(f(x)-y,inf);
end
figure(1)
semilogy(N,errLag,'r-*')
figure(2)
semilogy(N,errInt1,'r-*')

% per n+1=6 nodi, faccio un grafico in cui metto tutte le interpolazioni e
% approssimazioni di f richieste
n=5;
X0=linspace(a,b,n+1);
P=polyfit(X0,f(X0),n);
P1=polyfit(X0,f(X0),1);
P2=polyfit(X0,f(X0),3);
figure(4)

plot(x,f(x),x,polyval(P,x),x,interp1(X0,f(X0),x),x,polyval(P1,x),...
    x,polyval(P2,x),x,cubicspline(X0,f(X0),x));

legend('f esatta','Lagrange','interp1','LS p=1','LS p=2','spline')
hold on
plot(X0,f(X0),'go');
xlabel('x');
ylabel('y');
xlim([a b]);



